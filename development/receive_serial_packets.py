#!/usr/bin/python
#
# receive_serial_packets.py
# Read serial packets and print out the commands.
#
# Author : Eirik Taylor
# Date   : 28/09/2021
#
#=================================

import struct
import time
import sys
import signal
import subprocess
from fcntl import ioctl
import os
from ctypes import *
import struct

# Output device to send serial characters on
charDevicePath = '/dev/rs485_ch'

##################################################################################

# Capture interrupt signal (Ctrl-C). Close the TCP connection
def cleanupResourcesAndExit():
    print 'Shutting down...\n'
    os.close(fd)
    sys.exit(0)


##################################################################################

# Capture interrupt signal (Ctrl-C). Close the TCP connection
def handler(signum, frame):
    cleanupResourcesAndExit()

signal.signal(signal.SIGINT, handler)


##################################################################################

def shutdownEverything():
    command = '/usr/bin/sudo /sbin/shutdown -h now'
    process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
    output = process.communicate()[0]
    print output
    cleanupResourcesAndExit()

##################################################################################

class ControlPacket:

    def __init__(self):
        self.RecordingState = 0
        self.SystemShutdownRequest = 0
        self.RovTranslateX = 0
        self.RovRotateYaw = 0
        self.RovTranslateY = 0
        self.RovTranslateZ = 0
        self.MainLights = 0
        self.CameraRotateYaw = 0
        self.CameraRotatePitch = 0

    def __init__(self, byteArray):
        if len(byteArray) != 6 :
            raise ValueError('Incorrect number of elements in byteArray')

        self.RecordingState = byteArray[0]
        self.SystemShutdownRequest = byteArray[1]
        self.RovTranslateX = byteArray[3]
        self.RovRotateYaw = byteArray[5]
        self.RovTranslateY = 0
        self.RovTranslateZ = byteArray[4]
        self.MainLights = byteArray[2]
        self.CameraRotateYaw = 0
        self.CameraRotatePitch = 0

    def getEntireState(self):
        return ("<SET_VIDEO_RECORD:{};SET_LIGHT:{};ROTATE_ROV_YAW:{}; \
            TRANSLATE_ROV_X:{};TRANSLATE_ROV_Y:{}; \
            TRANSLATE_ROV_Z:{};ROTATE_CAMERA_YAW:{}; \
            ROTATE_CAMERA_PITCH:{};SYSTEM_SHUTDOWN:0;>".format(
                self.RecordingState,
                self.MainLights,
                self.RovRotateYaw,
                self.RovTranslateX,
                self.RovTranslateY,
                self.RovTranslateZ,
                self.CameraRotateYaw,
                self.CameraRotatePitch,
                self.SystemShutdownRequest))

    def getSerialPacket(self):
        # See https://eli.thegreenplace.net/2009/08/12/framing-in-serial-communications

        # Parse into framed serial message
        # Should maybe add error correction as well
        Start_flag = 0x12
        End_flag = 0x13
        Escape_flag = 0x7D

        serialDataPacket = [
            self.RecordingState,
            self.SystemShutdownRequest,
            self.MainLights,
            self.RovTranslateX + 127,
            self.RovTranslateZ + 127,
            self.RovRotateYaw + 127]

        # Add escape characters to data
        i = 0
        while i < len(serialDataPacket):
            if (serialDataPacket[i] == Start_flag) or \
            (serialDataPacket[i] == End_flag) or \
            (serialDataPacket[i] == Escape_flag) :
                serialDataPacket.insert(i, Escape_flag)
                i += 2
            else :
                i += 1

        # Insert start and end flags
        serialDataPacket.insert(0, Start_flag)
        serialDataPacket.insert(len(serialDataPacket), End_flag)

        return serialDataPacket


##################################################################################

def createControlPacketFromSerialStream(fileDescriptor):
    # See https://eli.thegreenplace.net/2009/08/12/framing-in-serial-communications

    # Definitions
    Start_flag = 0x12
    End_flag = 0x13
    Escape_flag = 0x7D
    WAIT_HEADER, IN_MSG, AFTER_ESC = range(3)
    receviedData = []

    # Parsing loop
    state = WAIT_HEADER
    while True:
        rawDataRead = os.read(fileDescriptor, 1)
        # Convert from bytestring to actual data
        readByte = struct.unpack('>b', rawDataRead)[0]

        # Go through reception state machine
        if state == WAIT_HEADER:
            # Start receiving!
            if readByte == Start_flag:
                #print('New message start!')
                state = IN_MSG
            #else:
                #print('Discarded byte while waiting for header: {}'.format(readByte))

        elif state == IN_MSG:
            # Check for special characters
            if readByte == Start_flag:
                raise EOFError('ERROR in createControlPacketFromSerialStream, format error!')
            elif readByte == End_flag:
                #print('Message end')
                return receviedData
            elif readByte == Escape_flag:
                #print('Escape flag detected')
                state = AFTER_ESC
            else:
                #print('Adding data')
                receviedData.append(readByte)

        elif state == AFTER_ESC:
            #print('Adding escaped data')
            receviedData.append(readByte)
            state = IN_MSG

        else:
            raise EOFError('ERROR in createControlPacketFromSerialStream, invalid state!')



##################################################################################

# Attempt to initialize the serial device
fd = os.open(charDevicePath, os.O_RDWR)

#define RS485IOC_SETBAUD 100
RS485IOC_BASE = 2147771292
BAUD_RATE_INDEX = RS485IOC_BASE + 100

print 'Setting baudrate with ioctl'
baudRateValue = 112500
baudRateAsMemoryObject = struct.pack("L", baudRateValue)
ioctl(fd, BAUD_RATE_INDEX, baudRateAsMemoryObject)

# TODO: Should perhaps have an ioctl for emptying the software buffer.
# Alternatively have the open call clear the buffer?
# Empty the buffer before starting. It's software size is 512 bytes.
buffer = os.read(fd, 512)

##############################################################

print 'Reading char by char from serial port'

# Main thread handles the socket connection
while True:
    try:
        # Read from the serial port and pack into messages
        dataPacket = createControlPacketFromSerialStream(fd)
        #print ("Serial raw data: {}".format(dataPacket))

        controlPacket = ControlPacket(dataPacket)
        # Show what we've got
        print ('Control packet: {}'.format(controlPacket.getEntireState()))

        if (controlPacket.SystemShutdownRequest != 0):
            print('Shutdown requested!')
            break;

    except EOFError as err:
        print('Handling run-time error:', err)
    except ValueError as err:
        print('Handling run-time error:', err)

################################################################
# Landing here means the loop has been broken, exit!

cleanupResourcesAndExit()   # Debugging, only exit the script
#shutdownEverything()
