#!/usr/bin/python
#
# rov_core_control_serial.py
# Read serial packets and control the ROV Brain Board
#
# Author : Eirik Taylor
# Date   : 29/09/2021
#
#=================================

import struct
import time
import sys
import signal
import subprocess
from fcntl import ioctl
import os
from ctypes import *
import struct
import smbus
import ms5837
from datetime import timedelta
import os, errno
import os.path
import picamera
import psutil

# Low disk space warning
lowDiskSpaceLimitGb = 5.0

# Motor Tuning
rightServoCenterOffset = 7	# of 0 - 255 range
leftServoCenterOffset = 7	# of 0 - 255 range

# Output device to send serial characters on
charDevicePath = '/dev/rs485_ch'

# ROV Brain Board I2C Configuration
bus = smbus.SMBus(1)
address = 0x03

# Define the I2C control registers
MOTOR_SERVO_0 = 0
MOTOR_SERVO_1 = 1
LED_PWM_0 = 2
LED_PWM_1 = 3
TWI_DATA_ESC_EN = 4
TWI_DATA_BATCRIT_OVERRIDE = 5

##################################################################################

# Capture interrupt signal (Ctrl-C). Close the TCP connection
def cleanupResourcesAndExit():
    print 'Shutting down...\n'
    resetBrainBoard()
    os.close(fd)
    if (currentRecordingState == 1):
        camera.stop_recording(splitter_port=0)
    sys.exit(0)


##################################################################################

# Capture interrupt signal (Ctrl-C). Close the TCP connection
def handler(signum, frame):
    cleanupResourcesAndExit()

signal.signal(signal.SIGINT, handler)


##################################################################################

def shutdownEverything():
    command = '/usr/bin/sudo /sbin/shutdown -h now'
    process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
    output = process.communicate()[0]
    print output
    cleanupResourcesAndExit()


##################################################################################

# ROV Brain board specific stuff
def resetBrainBoard():
    bus.write_byte_data(address, TWI_DATA_ESC_EN, 0)
    bus.write_byte_data(address, TWI_DATA_BATCRIT_OVERRIDE, 0)
    bus.write_byte_data(address, LED_PWM_0, 0)
    bus.write_byte_data(address, LED_PWM_1, 0)
    bus.write_byte_data(address, MOTOR_SERVO_0, 127)
    bus.write_byte_data(address, MOTOR_SERVO_1, 127)
    print("Reset states, and turned OFF ESC power.")

def getBatteryVoltage():
    # Returns the voltage as a percentage
    voltageEightBit = bus.read_byte_data(address, 0)
    return 100.0 * (voltageEightBit / 255.0)

def isHallEffectSensorTriggered():
    # Returns a true/false value indicating whether a magnet is sensed near the sensor
    # This sensor is tied to a reset line on the Raspberry Pi, so if a magnet is present,
    # this code certainly won't notice!
    hallEffectLevel = bus.read_byte_data(address, 1)
    # TODO:	If the level deviates significantly from 127, then a magnet must be present
    return hallEffectLevel

def initializeRovBrainBoardAndCalibrateMotors():
    print("Set initial states, and turned ON ESC power.")

    # Write the battery level low flag override, resetting any battery level related stops
    bus.write_byte_data(address, TWI_DATA_BATCRIT_OVERRIDE, 1)

    # Set the LEDs to low power
    bus.write_byte_data(address, LED_PWM_0, 40)
    bus.write_byte_data(address, LED_PWM_1, 40)

    # Set motors to center position
    bus.write_byte_data(address, MOTOR_SERVO_0, 127)
    bus.write_byte_data(address, MOTOR_SERVO_1, 127)
    # Enable ESC power, and wait for power up
    bus.write_byte_data(address, TWI_DATA_ESC_EN, 255)
    time.sleep(2)

    # Run motor arming sequence
    for output in range(127, 134):
    	print("Setting motors to %d" % (output))
    	bus.write_byte_data(address, MOTOR_SERVO_0, output)
    	bus.write_byte_data(address, MOTOR_SERVO_1, output)
    	time.sleep(0.1)

    print("Set motors to 50%")
    bus.write_byte_data(address, MOTOR_SERVO_0, 127)
    bus.write_byte_data(address, MOTOR_SERVO_1, 127)
    bus.write_byte_data(address, LED_PWM_0, 0)
    bus.write_byte_data(address, LED_PWM_1, 0)
    time.sleep(3)

##################################################################################

class ControlPacket:

    def __init__(self):
        self.RecordingState = 0
        self.SystemShutdownRequest = 0
        self.RovTranslateX = 0
        self.RovRotateYaw = 0
        self.RovTranslateY = 0
        self.RovTranslateZ = 0
        self.MainLights = 0
        self.CameraRotateYaw = 0
        self.CameraRotatePitch = 0

    def __init__(self, byteArray):
        if len(byteArray) != 6 :
            raise ValueError('Incorrect number of elements in byteArray')

        self.RecordingState = byteArray[0]
        self.SystemShutdownRequest = byteArray[1]
        self.RovTranslateX = byteArray[3]
        self.RovRotateYaw = byteArray[5]
        self.RovTranslateY = 0
        self.RovTranslateZ = byteArray[4]
        self.MainLights = byteArray[2]
        self.CameraRotateYaw = 0
        self.CameraRotatePitch = 0

    def getEntireState(self):
        return ("<SET_VIDEO_RECORD:{};SET_LIGHT:{};ROTATE_ROV_YAW:{}; \
            TRANSLATE_ROV_X:{};TRANSLATE_ROV_Y:{}; \
            TRANSLATE_ROV_Z:{};ROTATE_CAMERA_YAW:{}; \
            ROTATE_CAMERA_PITCH:{};SYSTEM_SHUTDOWN:0;>".format(
                self.RecordingState,
                self.MainLights,
                self.RovRotateYaw,
                self.RovTranslateX,
                self.RovTranslateY,
                self.RovTranslateZ,
                self.CameraRotateYaw,
                self.CameraRotatePitch,
                self.SystemShutdownRequest))

    def getSerialPacket(self):
        # See https://eli.thegreenplace.net/2009/08/12/framing-in-serial-communications

        # Parse into framed serial message
        # Should maybe add error correction as well
        Start_flag = 0x12
        End_flag = 0x13
        Escape_flag = 0x7D

        serialDataPacket = [
            self.RecordingState,
            self.SystemShutdownRequest,
            self.MainLights,
            self.RovTranslateX + 127,
            self.RovTranslateZ + 127,
            self.RovRotateYaw + 127]

        # Add escape characters to data
        i = 0
        while i < len(serialDataPacket):
            if (serialDataPacket[i] == Start_flag) or \
            (serialDataPacket[i] == End_flag) or \
            (serialDataPacket[i] == Escape_flag) :
                serialDataPacket.insert(i, Escape_flag)
                i += 2
            else :
                i += 1

        # Insert start and end flags
        serialDataPacket.insert(0, Start_flag)
        serialDataPacket.insert(len(serialDataPacket), End_flag)

        return serialDataPacket


##################################################################################

def to_unsigned(val, bits):
    # Convert a signed number to an equivilant unsigned number
    if val >= 0:
        return val
    else:
        return (1 << bits) + val

def createControlPacketFromSerialStream(fileDescriptor):
    # See https://eli.thegreenplace.net/2009/08/12/framing-in-serial-communications

    # Definitions
    Start_flag = 0x12
    End_flag = 0x13
    Escape_flag = 0x7D
    WAIT_HEADER, IN_MSG, AFTER_ESC = range(3)
    receviedData = []

    # Parsing loop
    state = WAIT_HEADER
    while True:
        rawDataRead = os.read(fileDescriptor, 1)
        # Convert from bytestring to actual data
        readByte = struct.unpack('>b', rawDataRead)[0]
        readByte = to_unsigned(readByte, 8)

        # Go through reception state machine
        if state == WAIT_HEADER:
            # Start receiving!
            if readByte == Start_flag:
                #print('New message start!')
                state = IN_MSG
            #else:
                #print('Discarded byte while waiting for header: {}'.format(readByte))

        elif state == IN_MSG:
            # Check for special characters
            if readByte == Start_flag:
                raise EOFError('ERROR in createControlPacketFromSerialStream, format error!')
            elif readByte == End_flag:
                #print('Message end')
                return receviedData
            elif readByte == Escape_flag:
                #print('Escape flag detected')
                state = AFTER_ESC
            else:
                #print('Adding data')
                receviedData.append(readByte)

        elif state == AFTER_ESC:
            #print('Adding escaped data')
            receviedData.append(readByte)
            state = IN_MSG

        else:
            raise EOFError('ERROR in createControlPacketFromSerialStream, invalid state!')



##################################################################################

def convertToFloatPercentage(input, limit):
    # Convert an input to a float between 0.0 and 1.0
    return float(input) / float(limit)

def convertToExpFloatPercentage(input, limit):
    percentage = convertToFloatPercentage(input, limit)
    return percentage * percentage

def convertToFloatPercentageCenter(input, limit):
    # Convert an input to a float between -1.0 and 1.0
    return 2.0 * (convertToFloatPercentage(input, limit) - 0.5)

def controlBrainBoardBasedOnControlPacket(controlPacket):
    actualBrightness = controlPacket.MainLights
    print ('LED brightness raw: {}'.format(actualBrightness))
    if (actualBrightness > 0):
        # Ensure 0 brightness remains zero, otherwise tune for a nice brightness curve
        actualBrightness = convertToExpFloatPercentage(actualBrightness, 255)
        minPower = 15
        actualBrightness = minPower + (actualBrightness * (255 - minPower))
    print ('Improved LED brightness curve: {}'.format(actualBrightness))
    
    print ('Motor Yaw raw: {}'.format(controlPacket.RovRotateYaw))
    print ('Motor Thrust raw: {}'.format(controlPacket.RovTranslateX))

    desiredROVYaw = convertToFloatPercentageCenter(controlPacket.RovRotateYaw, 255)
    # Scale down the effect of yaw
    desiredROVYaw = desiredROVYaw * 0.5
    desiredROVThrust = -convertToFloatPercentageCenter(controlPacket.RovTranslateX, 255)
    print ('Motor Yaw: {}'.format(desiredROVYaw))
    print ('Motor Thrust: {}'.format(desiredROVThrust))

    # Determine thrust in [1, -1] space
    MotorRightThrust = -desiredROVYaw
    MotorLeftThrust = desiredROVYaw
    MotorRightThrust = MotorRightThrust + desiredROVThrust
    MotorLeftThrust = MotorLeftThrust + desiredROVThrust
    if (MotorRightThrust > 1.0):
        MotorRightThrust = 1.0
    elif (MotorRightThrust < -1.0):
        MotorRightThrust = -1.0
    if (MotorLeftThrust > 1.0):
        MotorLeftThrust = 1.0
    elif (MotorLeftThrust < -1.0):
        MotorLeftThrust = -1.0

    print ('Motor Right [1, -1]: {}'.format(MotorRightThrust))
    print ('Motor Left [1, -1]: {}'.format(MotorLeftThrust))

    # Map to [0, 1] space
    MotorRightThrust = MotorRightThrust + 1.0
    MotorLeftThrust = MotorLeftThrust + 1.0
    MotorRightThrust = MotorRightThrust / 2.0
    MotorLeftThrust = MotorLeftThrust / 2.0

    # Finally convert to 8-bit space
    MotorRightThrust = MotorRightThrust * 255
    MotorLeftThrust = MotorLeftThrust * 255

    # The center positions of the servos seem off
    MotorRightThrust = MotorRightThrust + rightServoCenterOffset
    MotorLeftThrust = MotorLeftThrust + leftServoCenterOffset
    if (MotorRightThrust > 255):
        MotorRightThrust = 255
    elif (MotorRightThrust < 0):
        MotorRightThrust = 0
    if (MotorLeftThrust > 255):
        MotorLeftThrust = 255
    elif (MotorLeftThrust < 0):
        MotorLeftThrust = 0

    print ('Motor Right: {}'.format(MotorRightThrust))
    print ('Motor Left: {}'.format(MotorLeftThrust))

    # Output!
    bus.write_byte_data(address, LED_PWM_0, int(actualBrightness))
    bus.write_byte_data(address, LED_PWM_1, int(actualBrightness))
    bus.write_byte_data(address, MOTOR_SERVO_0, int(MotorRightThrust))
    bus.write_byte_data(address, MOTOR_SERVO_1, int(MotorLeftThrust))


####################### Helper function to format time stamp ##############################

def printNiceTimeDelta(stime, etime):
    delay = timedelta(seconds=(etime - stime))
    if (delay.days > 0):
        out = str(delay).replace(" days, ", ":")
    else:
        out = "0:" + str(delay)
    outAr = out.split(':')
    outAr = ["%02d" % (int(float(x))) for x in outAr]
    out   = ":".join(outAr[1:])
    return " " + out

####################### Set up sensor connection ######################################

def initializeDepthSensor():
    depthSensor = ms5837.MS5837_02BA(1)  # Sensor on I2C bus 1
    if depthSensor.init():
        depthSensor.setFluidDensity(ms5837.DENSITY_SALTWATER)
        return depthSensor
    else:
        return None

####################### Recording file handling #######################################

def getNextFileNameForVideoRecording():
    # Determine the file being saved to
    storageDirectory = '/home/pi/capturedVideo/'
    if not os.path.exists(storageDirectory):
        os.makedirs(storageDirectory)

    # Find the first available name
    fileIndex = 0
    fileName = 'rov_video'
    extension = '.h264'

    fullVideoFilePath = storageDirectory + fileName + str(fileIndex) + extension
    while os.path.exists(fullVideoFilePath):
        fileIndex = fileIndex + 1
        fullVideoFilePath = storageDirectory + fileName + str(fileIndex) + extension

    return fullVideoFilePath

####################### Navigation camera handling #######################################

def initializeCamera():
    # initialize the camera, resolution 1640x1232, Full FOV, up to 40fps
    # See https://picamera.readthedocs.io/en/release-1.13/fov.html?highlight=field%20of%20view#sensor-modes
    camera = picamera.PiCamera(sensor_mode=4, framerate=25)
    camera.resolution = (1640, 1232)
    camera.hflip = True
    camera.vflip = True
    camera.rotation = 90
    camera.awb_mode = 'flash'
    camera.brightness = 60
    camera.contrast = 0
    camera.drc_strength = 'off'

    # Annotations
    camera.annotate_background = picamera.Color('white')
    camera.annotate_foreground = picamera.Color('black')
    camera.annotate_text_size = 54
    return camera

####################### RS-485 Interface #######################################

def setBaudrate(baudrateToSet):
    #define RS485IOC_SETBAUD 100
    setIntWithIoctl(2147771392, baudrateToSet)

def setSpiClkRate(spiClkRateToSet):
    #define RS485IOC_SETBAUD 101
    setIntWithIoctl(2147771648, spiClkRateToSet)

def flushBuffers():
    #define RS485IOC_FLUSH 102
    setIntWithIoctl(2147771904, 0)

def setIntWithIoctl(index, intData):
    dataAsMemoryObject = struct.pack("L", intData)
    ioctl(fd, index, dataAsMemoryObject)
    print("Setting ioctl index %d with data %d" % (index, intData))

####################### Control script start #######################################

def getRemainingDiskSpace():
    diskUsage = psutil.disk_usage('/')
    remainingSpace = float(diskUsage.free / (1024.0 ** 3))
    return remainingSpace

####################### Control script start #######################################



# Attempt to initialize the serial device
fd = os.open(charDevicePath, os.O_RDWR)

print 'Setting RS-485 parameters with ioctl'
setBaudrate(112500)
setSpiClkRate(18000000)
flushBuffers()

# Prepare the brain board for commands!
print 'Initializing ROV Brain board'
initializeRovBrainBoardAndCalibrateMotors()

# Connect to the depth sensor!
print 'Initializing depth sensor'
depthSensor = initializeDepthSensor()

# Start the camera
print 'Initializing camera'
camera = initializeCamera()
camera.start_preview()

# Check disk
print 'Remaining disk space:', getRemainingDiskSpace(), 'GB'

##############################################################

print 'Starting main control loop'
currentRecordingState = 0
startTime = time.time()

lowDiskSpaceWarning = False

# Main thread handles the socket connection
while True:
    try:
        # Read from the serial port and pack into messages
        dataPacket = createControlPacketFromSerialStream(fd)
        #print ("Serial raw data: {}".format(dataPacket))

        controlPacket = ControlPacket(dataPacket)
        # Show what we've got
        #print ('Control packet: {}'.format(controlPacket.getEntireState()))

        if (controlPacket.SystemShutdownRequest != 0):
            print('Shutdown requested!')
            break;

        elif (controlPacket.RecordingState != currentRecordingState):
            # Update disk usage
            if (getRemainingDiskSpace() < lowDiskSpaceLimitGb):
                lowDiskSpaceWarning = True
            else:
                lowDiskSpaceWarning = False
            # Switch recording mode
            currentRecordingState = controlPacket.RecordingState
            if (controlPacket.RecordingState == 1):
                print('Starting recording!')
                fullVideoFilePath = getNextFileNameForVideoRecording()
                print 'Saving video to file: ' + fullVideoFilePath
                camera.start_recording(fullVideoFilePath, format='h264', bitrate=8200000, splitter_port=0)
            else:
                print('Starting regular video feed.')
                camera.stop_recording(splitter_port=0)

        controlBrainBoardBasedOnControlPacket(controlPacket)

        # TODO: Should check how the recording is proceeding with wait_recording
        # This blocks I think, so it would bog down the control communications
        #if currentRecordingState == 1:
        #    camera.wait_recording(timeout=0.2, splitter_port=0)

        # Get elapsed time
        displayText = printNiceTimeDelta(startTime, time.time())
        # Append disk warning
        if (lowDiskSpaceWarning):
            displayText += " | Disk full"
        # Get battery level
        batteryLevel = getBatteryVoltage()
        # Update sensor data
        if (depthSensor != None) and depthSensor.read():
            temperature = depthSensor.temperature(ms5837.UNITS_Centigrade)
            saltwaterDepth = depthSensor.depth()
            displayText += " | %.1fm | %.1fC | %.0f%% Bat" % (saltwaterDepth, temperature, batteryLevel)
        else:
            displayText += " | Sensor Error"
        # Add recording indicator
        if (currentRecordingState == 0):
            displayText += " | Not Rec"
            
        camera.annotate_text = displayText

    except EOFError as err:
        print('Handling run-time error:', err)
    except ValueError as err:
        print('Handling run-time error:', err)

################################################################
# Landing here means the loop has been broken, exit!

#cleanupResourcesAndExit()   # Debugging, only exit the script
shutdownEverything()
