# README #

Control software for an ROV core (underwater) unit. Control signals from the tether are sent over a RS-485 line, and video feed is sent via the analog output. PiCamera is used for image capture and on screen display. Script is intended to run at boot time.

### How do I get set up? ###

* This software was made specifically to run on a raspberry pi zero w, but should be adaptable to other platforms. Copy this entire directory to the home folder of the pi user. Should work more out of the box, by following the instructions in the README folder to set up. The Raspberry Pi should have an Internet connection for the initial setup procedure, in order to download required packages.

Keep in mind the rs485_ch.ko module is compiled for a specific kernel version, and will probably not work without being recompiled from source, against the header files of the kernel version currently in use.

### Contribution guidelines ###

* Feel free to come with suggestions! Currently looking into sending video feed over the RS-485 link.

### Who do I talk to? ###

* Eirik Taylor @ uzzors2k.com
