#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>

#include "rs485_ioctl.h"

#define DEVICE_FILE_NAME "/dev/rs485_ch"

#define CMD_NONE 0
#define CMD_BAUDRATE 1
#define CMD_SPI_CLK 2

/*
    Sets all ioctl commands related to the character driver.
    Defaults are in place, but can be overriden using command
    line arguments.

*/
int main(int argc, char **argv)
{
    int file_desc = 0;
    int ret_val = 0;

    int baudrate = 115200;
    int spiClockRate = 1000000;
    int dummy = 0;

    // Read in command line arguments
    int argumentState = CMD_NONE;
    for (int i = 1; i < argc; i++)
    {
        switch (argumentState)
        {
            case CMD_NONE:
            {
                if (strcmp(argv[i], "-b") == 0)
                {
                    argumentState = CMD_BAUDRATE;
                }
                else if (strcmp(argv[i], "-s") == 0)
                {
                    argumentState = CMD_SPI_CLK;
                }
                else if (strcmp(argv[i], "-h") == 0)
                {
                    // Display help and terminate
                    printf("Command line tool for setting parameters of rs485_ch driver.\n");
                    printf("Will set default parameters of Baurdate 115200, and SPI clock rate of 1MHz\n");
                    printf("\nValid commands for overriding defaults are:\n");
                    printf("  -b <baudrate>\n");
                    printf("  -s <SPI clock rate>\n");
                    return 0;
                }
                else
                {
                    printf("Unrecognized command %s\n", argv[i]);
                }
                break;
            }
            case CMD_BAUDRATE:
            {
                baudrate = atoi(argv[i]);
                printf("Baudrate %d\n", baudrate);
                argumentState = CMD_NONE;
                break;
            }
            case CMD_SPI_CLK:
            {
                spiClockRate = atoi(argv[i]);
                printf("SPI Clock %d Hz\n", spiClockRate);
                argumentState = CMD_NONE;
                break;
            }
        }
    }

    // Open device, and apply all commands
    file_desc = open(DEVICE_FILE_NAME, 0);
    if (file_desc < 0) {
        printf("ERROR: Can't open device file: %s\n", DEVICE_FILE_NAME);
        return -1;
    }

    ret_val = ioctl(file_desc, RS485IOC_SETBAUD, &baudrate);
    ret_val = ioctl(file_desc, RS485IOC_SETSPICLK, &spiClockRate);
    ret_val = ioctl(file_desc, RS485IOC_FLUSH, 0);

    close(file_desc);

    if (ret_val < 0) {
        printf("ERROR: ioctl called failed with return code: %d\n", ret_val);
    }

    printf("Set /dev/rs485_ch baudrate to: %d\n", baudrate);

    return 0;
}
