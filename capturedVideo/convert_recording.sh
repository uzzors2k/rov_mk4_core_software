#!/bin/bash

# Specify the video file name by writing them as a space-delimited list following the script file name.
# Inside the script, the $1 variable references the first argument in the command line, $2 the second
# argument and so forth. The variable $0 references to the current script.

sudo MP4Box -add rov_video$1.h264 rov_mp4_video$1.mp4

