#!/bin/bash

# Find the latest repositories
sudo apt-get update --allow-releaseinfo-change

# For the control script
sudo apt-get install -y gpac
sudo apt-get install -y python-smbus
sudo apt-get install -y python-picamera
sudo apt-get install -y python-psutil

