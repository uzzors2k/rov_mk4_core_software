
Once everything is installed, make the main scripts executable

chmod 755 rov_core_control_serial.py

Once they have been tested and everything is ready, 
add them to the crontab using crontab -e, with the sudo account.
Notice that the control script is set to start 20 seconds after reboot,
this is to ensure that the required bluetooth unit and character driver
are ready.

sudo crontab -e

@reboot sleep 20; /usr/bin/python2.7 /home/pi/rov_core_control_serial.py

