Welcome!

From raspi-config, setup the following:

1) System Options -> Hostname -> Set to rovtether or rovcore
2) System Options -> Wait for Network on boot -> No
3) Advanced Options -> Expand Filesystem
4) Interfacing Options -> Enable Camera
5) Interfacing Options -> Enable I2C


Then setup PAL analog video output:

4) sudo nano /boot/config.txt

	Find the line with sdtv_mode, and set to 2 for regular PAL, or 0 for regular NTSC

	sdtv_mode=2

5) Run the script "install_dependencies.sh"
