
///////////////////////////////////////////////////////////

Chip type is:
ATmega168PB

///////////////////////////////////////////////////////////

Installation

.conf file entry
Install avrdude, and copy the .conf file to the home directory.

sudo apt-get install avrdude
cp /etc/avrdude.conf ~/avrdude_gpio.conf

Enter the following to create a new programmer:

# Linux GPIO configuration for avrdude.
# Change the lines below to the GPIO pins connected to the AVR.
programmer
  id    = "pi_0";
  desc  = "Use the Linux sysfs interface to bitbang GPIO lines";
  type  = "linuxgpio";
  reset = 27;
  sck   = 17;
  mosi  = 4;
  miso  = 18;
;

///////////////////////////////////////////////////////////


Commands:



Verify Connection

sudo avrdude -p atmega168pb -C ~/avrdude_gpio.conf -c pi_0 -v




Program File

sudo avrdude -p atmega168pb -C ~/avrdude_gpio.conf -c pi_0 -v -U flash:w:blink_test.hex:i

or

sudo avrdude -p atmega168pb -C ~/avrdude_gpio.conf -c pi_0 -v -U flash:w:RovBrainFirmware.hex:i



Program Fuses

sudo avrdude -p atmega168pb -C ~/avrdude_gpio.conf -c pi_0 -v -U lfuse:w:0xd7:m -U hfuse:w:0xdd:m -U efuse:w:0xf9:m 

Brown out detection at 2.7V, and CKSEL = 0111, SUT = 01. No divide by 8. Otherwise default.


///////////////////////////////////////////////////////////

Info:

https://www.ladyada.net/learn/avr/avrdude.html
https://learn.adafruit.com/program-an-avr-or-arduino-using-raspberry-pi-gpio-pins/configuration
https://www.engbedded.com/fusecalc/


///////////////////////////////////////////////////////////

