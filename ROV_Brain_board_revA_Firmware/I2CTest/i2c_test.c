/*
 * RovBrainFirmware.cpp
 *
 * I2C Interface to master (Setting of PWM outputs with slave W, and reading of battery voltage with slave R)
 * 10MHz crystal osc
 * Motor servo PWM signals on PB1 and PB2, Timer1
 * ADC with supply voltage reference
 * Analog hall effect sensor on PC0 (full scale)
 * Analog battery voltage measurement on PC1 (input 14.0 to 10.8, with 36k to 10k divider)
 * LED PWM (which frequency? on PD5 and PD6)
 * Active low master reset on PD2
 * Active high ESC enable on PD3
 * Status LED on PD4
 *
 * Created: 24.05.2021 17:46:31
 * Author : uzzors2k
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>

#define F_CPU 10000000
#include <util/delay.h>

// 2x Motor PWM, 2x LED PWM, and 1x ESC enable
#define MAX_TWI_RECEIVE_DATA 5

#define TWI_DATA_MTR_0 0
#define TWI_DATA_MTR_1 1
#define TWI_DATA_LED_0 2
#define TWI_DATA_LED_1 3
#define TWI_DATA_ESC_EN 4

#define TWI_DATA_READY 0
#define TWI_ERROR 1
#define TWI_SET_REG 2

volatile uint8_t twiReceiveIndex = 0;
volatile uint8_t twiSendIndex = 0;
volatile uint8_t twiRequestedAdr = 0;
volatile uint8_t twiRecData[MAX_TWI_RECEIVE_DATA];
volatile uint8_t twiStatus = 0;

ISR(TWI_vect)
{
	uint8_t statusCode = TWSR & 0xF8;
	switch (statusCode)
	{
		/////////////////////// Slave Receive Modes (W) ///////////////////////////
		case 0x60:
		// Own SLA+W has been received, ACK has been returned
		{
			twiStatus &=~(1 << TWI_DATA_READY);
			twiStatus |= (1 << TWI_SET_REG);
			twiReceiveIndex = 0;
			twiRequestedAdr = 0;
			break;
		}
			
		case 0x80:
		// Previously  addressed  with  own SLA+W, data has been received, ACK has been returned
		{
			if (twiStatus & (1 << TWI_SET_REG))
			{
				// Set the initial register for reads or writes
				twiRequestedAdr = TWDR;
				twiReceiveIndex = twiRequestedAdr;
				twiStatus &=~(1 << TWI_SET_REG);
				
				if (twiRequestedAdr >= MAX_TWI_RECEIVE_DATA)
				{
					twiStatus |= (1 << TWI_ERROR);
					
					// Requested address is outside of valid range
					TWCR &=~(1 << TWEA);
					TWCR |= (1 << TWINT);
				}
			}
			else if (twiReceiveIndex >= MAX_TWI_RECEIVE_DATA)
			{
				twiStatus |= (1 << TWI_ERROR);
				
				// Receive buffer is full, NACK
				TWCR &=~(1 << TWEA);
				TWCR |= (1 << TWINT);
			}
			else
			{
				twiRecData[twiReceiveIndex] = TWDR;
				twiReceiveIndex++;
			}
			
			break;
		}
			
		case 0xA0:
		// A STOP condition, or repeated START condition has been received,
		// while still addressed as a Slave
		{
			twiStatus |= (1 << TWI_DATA_READY);
			break;
		}
		
		/////////////////////// Slave Transmit Modes (R) ///////////////////////////
		
		case 0xA8:
		// Own SLA+R has been received, ACK has been returned
		{
			twiSendIndex = twiRequestedAdr;
			//TWDR = lastBatteryVoltageReading;
			TWDR = twiRecData[twiSendIndex];
			break;
		}
		
		case 0xB8:
		// Data byte in TWDR has been transmitted, ACK has been received
		{
			twiSendIndex++;
			
			//TWDR = lastHallEffectSensorReading;
			TWDR = twiRecData[twiSendIndex];
			
			// Last byte to send, NACK
			if (twiSendIndex >= (MAX_TWI_RECEIVE_DATA - 1))
			{
				TWCR &=~(1 << TWEA);
				TWCR |= (1 << TWINT);
			}
			
			return;
		}
		
		case 0xC0:
		// Data byte in TWDR has been transmitted, NOT ACK has been received
		{
			break;
		}
		
		case 0xC8:
		// Last data byte in TWDR has been transmitted
		// (TWEA  =  “0”), ACK has been received
		{
			break;
		}
		
		
		/////////////////////// Error /////////////////////////////////////////
		default:
		// Unhandled event, reset TWI status
		TWCR |= (1 << TWSTO);
		twiStatus |= (1 << TWI_ERROR);
		return;
	}
	
	TWCR |= (1 << TWINT)|(1 << TWEA);	// Clear interrupt, and ACK
}

void initTwi()
{
	// Start TWI as slave receive mode
	PRR &=~(1 << PRTWI);	// Allow TWI
	TWBR = 4;				// Bit rate divider of 4. Gives 70kHz when in master mode.
	
	TWSR |= (1 << TWPS1)|
			(0 << TWPS0);	// Prescaler of 16
	
	TWCR =	(1 << TWEA)|
			(1 << TWEN)|
			(1 << TWIE)|
			(0 << TWSTA)|
			(0 << TWSTO);	// Interrupts, and enable outputs and ACK
	
	TWAR =	(0x03 << 1)|
			(0 << TWGCE);	// Slave address 0x03. Don't ACK on general call.
}

void initGpio()
{
	// Setup GPIO pins
	DDRD |= (1 << PD4)|(1 << PD3);
	PORTD = 0;
}

int main(void)
{
	initTwi();
	
	// Test
	twiRecData[0] = 0xC8;
	twiRecData[1] = 0x78;
	twiRecData[2] = 0xFF;
	twiRecData[3] = 0xEF;
	twiRecData[4] = 0x5a;
	
	initGpio();
	
	sei();
	
    while (1) 
    {
		// Handle new I2C data commands
		if (twiStatus & (1 << TWI_DATA_READY))
		{
			// Flash LED
			PORTD |= (1 << PD4);
			_delay_ms(100);
			PORTD &= ~(1 << PD4);
			
			twiStatus &=~(1 << TWI_DATA_READY);
		}
	}
}
