#!/usr/bin/python
import smbus
import time
import signal
import sys

# Installation Notes:
# sudo apt-get install python-smbus

bus = smbus.SMBus(1)
address = 0x03

# Define the I2C control registers
MOTOR_SERVO_0 = 0
MOTOR_SERVO_1 = 1
LED_PWM_0 = 2
LED_PWM_1 = 3
TWI_DATA_ESC_EN = 4
TWI_DATA_BATCRIT_OVERRIDE = 5

def signal_handler(sig, frame):
	# Disable outputs
	print("")
	bus.write_byte_data(address, TWI_DATA_ESC_EN, 0)
	bus.write_byte_data(address, TWI_DATA_BATCRIT_OVERRIDE, 0)
	bus.write_byte_data(address, LED_PWM_0, 0)
	bus.write_byte_data(address, LED_PWM_1, 0)
	bus.write_byte_data(address, MOTOR_SERVO_0, 127)
	bus.write_byte_data(address, MOTOR_SERVO_1, 127)
	print("Reset states, and turned OFF ESC power.")
	sys.exit(0)

def getBatteryVoltage():
	# Returns the voltage as a percentage
	voltageEightBit = bus.read_byte_data(address, 0)
	return 100.0 * (voltageEightBit / 255.0)

def isHallEffectSensorTriggered():
	# Returns a true/false value indicating whether a magnet is sensed near the sensor
	# This sensor is tied to a reset line on the Raspberry Pi, so if a magnet is present,
	# this code certainly won't notice!
	hallEffectLevel = bus.read_byte_data(address, 1)
	# TODO:	If the level deviates significantly from 127, then a magnet must be present
	return hallEffectLevel

# Initialize the PWM board
print("Starting...")

signal.signal(signal.SIGINT, signal_handler)

print("Set initial states, and turned ON ESC power.")

# Write the battery level low flag override, resetting any battery level related stops
bus.write_byte_data(address, TWI_DATA_BATCRIT_OVERRIDE, 1)

# Set the LEDs to low power
bus.write_byte_data(address, LED_PWM_0, 40)
bus.write_byte_data(address, LED_PWM_1, 40)

# Set motors to center position
bus.write_byte_data(address, MOTOR_SERVO_0, 127)
bus.write_byte_data(address, MOTOR_SERVO_1, 127)
# Enable ESC power, and wait for power up
bus.write_byte_data(address, TWI_DATA_ESC_EN, 255)
time.sleep(2)

# Run motor arming sequence
for output in range(127, 133):
	print("Setting motors to %d" % (output))
	bus.write_byte_data(address, MOTOR_SERVO_0, output)
	bus.write_byte_data(address, MOTOR_SERVO_1, output)
	time.sleep(0.1)

print("Set motors to 50%")
bus.write_byte_data(address, MOTOR_SERVO_0, 127)
bus.write_byte_data(address, MOTOR_SERVO_1, 127)
time.sleep(3)

print("Starting automatic sequence...")
time.sleep(3)

# Monitor status, and slowly vary the ESC speed
counter = 3
direction = 1
while True:
	# Display battery voltage
	ledPowerLevel = counter * 42
	motorPowerLevel = (127 - 30) + (10 * counter)
	print("Battery level is %3.1f %%, setting LEDs to %d, motors to %d" % (getBatteryVoltage(), ledPowerLevel, motorPowerLevel))
	# Increment LED brightness
	bus.write_byte_data(address, LED_PWM_0, ledPowerLevel)
	bus.write_byte_data(address, LED_PWM_1, ledPowerLevel)
	# Step motor speed
	bus.write_byte_data(address, MOTOR_SERVO_0, motorPowerLevel)
	bus.write_byte_data(address, MOTOR_SERVO_1, motorPowerLevel)
	# Handle state counter
	if direction > 0 :
		counter = counter + 1
	else :
		counter = counter - 1
	# Handle overflow
	if counter > 5 :
		direction = -1
	elif counter < 1 :
		direction = 1
		# Wait for effect
	time.sleep(2)