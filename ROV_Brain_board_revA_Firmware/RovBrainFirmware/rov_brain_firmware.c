/*
 * RovBrainFirmware.cpp
 *
 * I2C Interface to master (Setting of PWM outputs with slave W, and reading of battery voltage with slave R)
 * 10MHz crystal osc
 * Motor servo PWM signals on PB1 and PB2, Timer1, (OC1A and OC1B)
 * ADC with supply voltage reference
 * Analog hall effect sensor on PC0 (full scale)
 * Analog battery voltage measurement on PC1 (input 14.0 to 10.8, with 36k to 10k divider)
 * LED PWM (which frequency? on PD5 and PD6, or OC0A and OC0B)
 * Active low master reset on PD2
 * Active high ESC enable on PD3
 * Status LED on PD4
 *
 * Created: 24.05.2021 17:46:31
 * Author : uzzors2k
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>

#define F_CPU 10000000
#include <util/delay.h>

#define TWI_SLAVE_ADR 0x03

// 2x Motor PWM, 2x LED PWM, and 1x ESC enable
#define MAX_TWI_RECEIVE_DATA 6

#define TWI_DATA_MTR_0 0
#define TWI_DATA_MTR_1 1
#define TWI_DATA_LED_0 2
#define TWI_DATA_LED_1 3
#define TWI_DATA_ESC_EN 4
#define TWI_DATA_BATCRIT_OVERRIDE 5

#define TWI_DATA_READY 0
#define TWI_ERROR 1
#define TWI_SET_REG 2

// I2C
volatile uint8_t twiReceiveIndex = 0;
volatile uint8_t twiSendIndex = 0;
volatile uint8_t twiRequestedAdr = 0;
volatile uint8_t twiRecData[MAX_TWI_RECEIVE_DATA];
volatile uint8_t twiStatus = 0;

// ADC
volatile uint8_t adcReadingChannel = 0;
volatile uint8_t lastBatteryVoltageReading = 0;
volatile uint8_t lastHallEffectSensorReading = 0;

// Servo PWM
volatile uint8_t updateServoOutputs = 0;

ISR(TWI_vect)
{
	uint8_t statusCode = TWSR & 0xF8;
	switch (statusCode)
	{
		/////////////////////// Slave Receive Modes (W) ///////////////////////////
		case 0x60:
		// Own SLA+W has been received, ACK has been returned
		{
			twiStatus &=~(1 << TWI_DATA_READY);
			twiStatus |= (1 << TWI_SET_REG);
			twiReceiveIndex = 0;
			twiRequestedAdr = 0;
			break;
		}
			
		case 0x80:
		// Previously  addressed  with  own SLA+W, data has been received, ACK has been returned
		{
			if (twiStatus & (1 << TWI_SET_REG))
			{
				// Set the initial register for reads or writes
				twiRequestedAdr = TWDR;
				twiReceiveIndex = twiRequestedAdr;
				twiStatus &=~(1 << TWI_SET_REG);
				
				if (twiRequestedAdr >= MAX_TWI_RECEIVE_DATA)
				{
					twiStatus |= (1 << TWI_ERROR);
					
					// Requested address is outside of valid range
					TWCR &=~(1 << TWEA);
					TWCR |= (1 << TWINT);
				}
			}
			else if (twiReceiveIndex >= MAX_TWI_RECEIVE_DATA)
			{
				twiStatus |= (1 << TWI_ERROR);
				
				// Receive buffer is full, NACK
				TWCR &=~(1 << TWEA);
				TWCR |= (1 << TWINT);
			}
			else
			{
				twiRecData[twiReceiveIndex] = TWDR;
				twiReceiveIndex++;
			}
			
			break;
		}
			
		case 0xA0:
		// A STOP condition, or repeated START condition has been received,
		// while still addressed as a Slave
		{
			twiStatus |= (1 << TWI_DATA_READY);
			break;
		}
		
		/////////////////////// Slave Transmit Modes (R) ///////////////////////////
		
		case 0xA8:
		// Own SLA+R has been received, ACK has been returned
		{
			twiSendIndex = twiRequestedAdr;
			if (twiRequestedAdr == 0)
			{
				TWDR = lastBatteryVoltageReading;
			}
			else
			{
				// Anything above, even invalid will give out the last byte in the train. NACK comes later.
				TWDR = lastHallEffectSensorReading;
			}
			break;
		}
		
		case 0xB8:
		// Data byte in TWDR has been transmitted, ACK has been received
		{
			twiSendIndex++;
			TWDR = lastHallEffectSensorReading;
			
			// Last byte to send, NACK
			TWCR &=~(1 << TWEA);
			TWCR |= (1 << TWINT);
			
			return;
		}
		
		case 0xC0:
		// Data byte in TWDR has been transmitted, NOT ACK has been received
		{
			break;
		}
		
		case 0xC8:
		// Last data byte in TWDR has been transmitted
		// (TWEA  =  “0”), ACK has been received
		{
			break;
		}
		
		
		/////////////////////// Error /////////////////////////////////////////
		default:
		// Unhandled event, reset TWI status
		TWCR |= (1 << TWSTO);
		twiStatus |= (1 << TWI_ERROR);
		return;
	}
	
	TWCR |= (1 << TWINT)|(1 << TWEA);	// Clear interrupt, and ACK
}

ISR(TIMER1_COMPA_vect) {
	// Allow servo timing sequence to start
	updateServoOutputs = 1;
}

uint16_t startAdcConversionOnChannel(uint8_t channel)
{
	//select ADC channel with safety mask
	ADMUX = (ADMUX & 0xF0) | (channel & 0x0F);
	
	//single conversion mode
	ADCSRA |= (1<<ADSC);
	
	// wait until ADC conversion is complete
	while( ADCSRA & (1<<ADSC) );
	
	return ADC;
}

void initTwi()
{
	// Start TWI as slave receive mode
	PRR &=~(1 << PRTWI);	// Allow TWI
	TWBR = 4;				// Bit rate divider of 4. Gives 70kHz when in master mode.
	
	TWSR |= (1 << TWPS1)|
			(0 << TWPS0);	// Prescaler of 16
	
	TWCR =	(1 << TWEA)|
			(1 << TWEN)|
			(1 << TWIE)|
			(0 << TWSTA)|
			(0 << TWSTO);	// Interrupts, and enable outputs and ACK
	
	TWAR =	(TWI_SLAVE_ADR << 1)|
			(0 << TWGCE);	// Set the slave address. Don't ACK on general call.
}

void initAdc()
{
	// Select Vref=AVcc
	ADMUX |= (1<<REFS0);
	
	// Init the ADC
	ADCSRA = (1 << ADEN)|
			(1 << ADPS2)|
			(1 << ADPS1)|
			(1 << ADPS0);	// Enable with prescaler of 128
}

void initGpio()
{
	// Setup GPIO pins
	DDRD |= (1 << PD6)|(1 << PD5)|(1 << PD4)|(1 << PD3);
	PORTD = 0;
	
	DDRB |= (1 << PB1)|(1 << PB2); // PB1 and PB2 are now outputs
	PORTB = 0;
	
	// Set up Timer0 for PWM
	OCR0A = 0;
	OCR0B = 0;
	
	TCCR0A |= (1 << COM0A1)|(1 << COM0B1);	// set none-inverting mode
	TCCR0A |= (1 << WGM01) | (1 << WGM00);	// set fast PWM Mode
	TCCR0B |= (1 << CS01);					// set prescaler to 8, and start PWM
	
	// Initialize timer 1 for 20ms interrupts, used to set timing for servo update signals
	TCCR1B = (1<<WGM12)|(0<<CS12)|(1<<CS11)|(0<<CS10);	// CTC mode, 1:8 prescaler
	OCR1A = 25000;			// 20ms interval
	TIMSK1 = (1<<OCIE1A);
}

int main(void)
{
	initTwi();
	initAdc();
	initGpio();
	
	sei();
	
	uint8_t batteryLevelCritical = 0;
	
    while (1) 
    {
		///////////////////// Handle new I2C data commands ////////////////////////////
		cli();
		if (twiStatus & (1 << TWI_DATA_READY))
		{
			// TODO: Set Motor PWM based on twiRecData - should be handled below?
			
			if (twiRecData[TWI_DATA_BATCRIT_OVERRIDE] != 0)
			{
				// Reset the battery warning
				batteryLevelCritical = 0;
			}
			
			if (batteryLevelCritical != 0)
			{
				// Turn off all power consuming outputs
				OCR0A = 0;
				OCR0B = 0;
				PORTD &=~(1 << PD3);
			}
			else
			{
				// Set the LED PWM duty cycles
				OCR0A = twiRecData[TWI_DATA_LED_0];
				OCR0B = twiRecData[TWI_DATA_LED_1];
				
				// Enable or disable power to the ESCs
				if (twiRecData[TWI_DATA_ESC_EN])
				{
					PORTD |= (1 << PD3);
				}
				else
				{
					PORTD &=~(1 << PD3);
				}
			}
			
			twiStatus &=~(1 << TWI_DATA_READY);
		}
		sei();
		
		// Handle status LED
		if (twiStatus & (1 << TWI_ERROR))
		{
			// TODO: Blink LED in error pattern? Handle the LED using one of the timers perhaps
			PORTD |= (1 << PD4);
			_delay_ms(100);
			PORTD &=~(1 << PD4);
			
			// Clear error
			twiStatus &= ~(1 << TWI_ERROR);
		}
		/////////////////////////////////////////////////////////////////////////////
		
		
		/////////////////////////// Read Analog Sensors /////////////////////////////
		
		// Update ADC readings
		uint16_t adcReading = startAdcConversionOnChannel(0);
		lastHallEffectSensorReading = (adcReading >> 2);
		
		adcReading = startAdcConversionOnChannel(1);
		
		// NOTE:	See the spreadsheet for the calculations, but in this case
		//          we have 3V3 ref, 10-bit resolution, 36k/10k voltage divider,
		//          and a battery range of 10,4 - 14,0V
		//			This in turn means we can roughly convert the value directly
		//			to an 8-bit battery percentage.
		lastBatteryVoltageReading = (adcReading - 694);
		
		/////////////////////////////////////////////////////////////////////////////
		
		
		////////////////////////// Act on ADC readings //////////////////////////////
		// Act on ADC reading
		if ((lastHallEffectSensorReading < 115) || (lastHallEffectSensorReading > 145))
		{
			// Set PD2 as output and pull low if hall effect sensor value is below/above some threshold
			// Light LED constantly as long as reset is pulled low.
			// Enter a state when this happens, so the trigger is pulled low for 1 second or so
			DDRD |= (1 << PD2);
			PORTD &= ~(1 << PD2);
			
			// Wait 1 second, illuminating the LED in the mean-time
			PORTD |= (1 << PD4);
			_delay_ms(1000);
			PORTD &= ~(1 << PD4);
			
			// Release the reset line again
			PORTD |= (1 << PD2);
			DDRD &=~(1 << PD2);
		}
		
		if (lastBatteryVoltageReading < 20)
		{
			// Disable ESCs and LED output when battery voltage is critically low.
			batteryLevelCritical = 1;
		}
		
		/////////////////////////////////////////////////////////////////////////////
		
		
		////////////////////////// Update servo signals /////////////////////////////
		if (updateServoOutputs)
		{
			// Start creating the servo timing signal
			PORTB |= (1 << PB2)|(1 << PB1);
			_delay_us(1000);
			
			// Let it do it's work for the next 1000us
			uint8_t time = 0;
			do
			{
				// 1ms / 255 = 3,92µs
				// 10.0MHz * 3,92µs = 39
				// 29 clock cycles are 3,93µs, so waste exactly 29 clock cycles before the next loop
				// However, there is a bunch of overhead in the code, so this actually needs to be tuned
				// Keep in mind that compiled code also uses these registers, so be careful. R18 is free
				// to use from assembly code. For a complete description, see application note Atmel AT1886.
				asm volatile("LDI R18, 7");
				asm volatile("LOOP: DEC R18");
				asm volatile("BRNE LOOP");
				
				// Clear the output once it's time has elapsed
				cli();
				if (time >= twiRecData[TWI_DATA_MTR_0])
				{
					PORTB &= ~(1 << PB1);
				}
				if (time >= twiRecData[TWI_DATA_MTR_1])
				{
					PORTB &= ~(1 << PB2);
				}
				sei();
			} while (time++ != 255);
			
			PORTB &= ~((1 << PB2)|(1 << PB1));
			updateServoOutputs = 0;
		}
		
		/////////////////////////////////////////////////////////////////////////////
	}
}
