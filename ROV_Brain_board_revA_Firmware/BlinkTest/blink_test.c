/*
 * RovBrainFirmware.cpp
 *
 * I2C Interface to master (Setting of PWM outputs with slave W, and reading of battery voltage with slave R)
 * 10MHz crystal osc
 * Motor servo PWM signals on PB1 and PB2, Timer1
 * ADC with supply voltage reference
 * Analog hall effect sensor on PC0 (full scale)
 * Analog battery voltage measurement on PC1 (input 14.0 to 10.8, with 36k to 10k divider)
 * LED PWM (which frequency? on PD5 and PD6)
 * Active low master reset on PD2
 * Active high ESC enable on PD3
 * Status LED on PD4
 *
 * Created: 12.06.2021
 * Author : uzzors2k
 */ 

#include <avr/io.h>

#define F_CPU 10000000
#include <util/delay.h>

void initGpio()
{
	// Setup GPIO pins
	DDRD |= (1 << PD4)|(1 << PD3);
	PORTD = 0;
}

int main(void)
{
	initGpio();
	
	uint8_t ledStatus = 0;
	
    while (1) 
    {
		// Handle status LED
		if (ledStatus)
		{
			PORTD |= (1 << PD4);
		}
		else
		{
			PORTD &= ~(1 << PD4);
		}
		
		ledStatus = ~ledStatus;
		_delay_ms(1000);
	}
}

